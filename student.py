# @Time : 2021/6/26 17:13 
# @Author : rp
# @Software: PyCharm
class Student:

    def __init__(self, id, name, sex, english, java, python):
        self.id = id
        self.name = name
        self.sex = sex
        self.english = english
        self.java = java
        self.python = python

    def print_obj(self):
        print(object.__dict__)

