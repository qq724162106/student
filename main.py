from student import Student
import os

# 定义存储学生信息的列表
stuInfos = []
# 定义文件所在路径
filename = "D:/stu.txt"


def menu():
    print("==================学生信息管理系统===============")
    print("---------------------功能菜单-------------------")
    print("1、录入学生信息")
    print("2、查找学生信息")
    print("3、删除学生信息")
    print("4、修改学生信息")
    print("5、排序")
    print("6、统计学生人数")
    print("7、显示学生所有信息")
    print("0、退出系统")
    print("----------------------------------------------")

    print("请选择:")

    a = int(input())
    while True:
        if a in [0, 1, 2, 3, 4, 5, 6, 7]:
            if a == 0:
                answer = input("您确定要退出系统吗？y/n\n")
                if answer == 'y' or answer == 'Y':
                    print("退出成功")
                    break
                else:
                    continue
            if a == 1:
                insert()  # 录入学生信息
                menu()
                break

            if a == 2:
                findAll()  # 查找学生信息
                break
            if a == 3:
                delete()
                break
            if a == 4:
                modify()
                break
            if a == 5:
                sort()
                break
            if a == 6:
                total()
                break
            if a == 7:
                show()
                break
        else:
            print("输入有误，请重新进入")
            break


"""
使用字典存储学生
"""


# 录入学生信息
def insert():
    print("录入学生信息")

    while True:
        print("请输入学生学号")
        id = input()
        print("请输入学生姓名")
        name = input()
        print("请输入学生性别")
        sex = input()
        print("请输入学生java成绩")
        java = float(input())
        print("请输入学生英语成绩")
        english = float(input())
        print("请输入学生python成绩")
        python = float(input())
        stu = {"id": id, "name": name, "sex": sex, "java": java, "english": english, "python": python}
        # stu = Student(id, name, sex, english, java, python)
        stuInfos.append(stu)
        answer = input("是否继续添加y/n")
        if answer == 'y':
            continue
        else:
            break
    # 将学生信息保存到文件中
    save(stuInfos)


# 将数据保存到文件中
def save(lst):
    stu_txt = open(filename, 'a', encoding="utf-8")
    # 遍历lst将数据写入文件
    for item in lst:
        stu_txt.write(str(item))
    stu_txt.close()


# 根据id查找学生信息
def findAll():
    studentInfo = []
    while True:
        id = input("请输入要查找的学生id:")
        if id != "":
            # 判断文件是否存在
            if os.path.exists(filename):
                stu_txt = open(filename, 'r', encoding="utf-8")
                student = stu_txt.readlines()
                stu_txt.close()
            else:
                student = []
            if student:
                stu_txt = open(filename, 'r', encoding="utf-8")
                d = {}
                for item in student:
                    d = dict(eval(item))
                    studentInfo.append(d)
                if d['id'] == id:
                    formatInfos(studentInfo)


# 根据id修改学生信息
def modify():
    print("修改学生信息")
    while True:
        id = input("请输入想要修改的学生id：")
        if id != '':
            # 判断文件是否存在
            if os.path.exists(filename):
                stu_txt = open(filename, 'r', encoding="utf-8")
                # readlines()功能为读取文件整行数据并转换为列表
                stu_old = stu_txt.readlines()
                stu_txt.close()
            else:
                # 文件不存在的情况下列表为空
                stu_old = []
            if stu_old:
                stu_txt = open(filename, 'w', encoding="utf-8")
                d = {}
                # 遍历从文件中读取的内容，并将该内容转换为字典
                for item in stu_old:
                    d = dict(eval(item))
                    # 修改完学生的信息然后存入到文件中
                    if d['id'] == id:
                        print("找到该学生，可以修改该学生的信息")
                        d['java'] = float(input("请输入修改后的java成绩："))
                        d['english'] = float(input("请输入修改后的english成绩："))
                        d['python'] = float(input("请输入修改后的python成绩："))
                        stu_txt.write(str(d))
                        stu_txt.close()

                        # 如果输入的id和读取的id不一致，将原学生信息存入到文件中
                    else:
                        stu_txt.write(str(d))
                        stu_txt.close()
                        break
            else:
                print("无学生信息")
            print("修改成功")
            answer = input("请问是否继续更改y/n")
            if answer == 'y':
                continue
            else:
                break


# 删除学生信息
def delete():
    print("删除学生信息")

    while True:
        print("请输入要删除的学生id")
        id = input()
        if id != '':
            # 判断文件是否存在
            if os.path.exists(filename):
                stu_txt = open(filename, 'r', encoding="utf-8")
                # readlines()功能为读取文件整行数据并转换为列表
                stu_old = stu_txt.readlines()
                stu_txt.close()
            else:
                # 文件不存在的情况下列表为空
                stu_old = []
                flag = False
                # 如果列表为空则返回false 否则返回true
            if stu_old:
                stu_txt = open(filename, 'w', encoding="utf-8")
                d = {}
                for item in stu_old:
                    # 将字符串转换为字典
                    d = dict(eval(item))
                    # 如果想要删除的id和文件中的id不一致就把该信息写入到文件中
                    if d['id'] != id:
                        stu_txt.write(str(d))
                    else:
                        flag = True
                if flag:
                    print("学号为" + id + "的学生信息已经删除")
                else:
                    print("没有找到id为{}的学生信息")
                    break
            else:
                print("无学生信息")
            show()
            answer = input("请问是否继续删除y/n")
            if answer == 'y':
                continue
            else:
                break


# 根据学生成绩进行排序
def sort():
    show()
    # 判断文件是否存在
    if os.path.exists(filename):
        stu_txt = open(filename, 'r', encoding="utf-8")
        student_list = stu_txt.readlines()
        student_new = []
        for item in student_list:
            d = dict(eval(item))
            student_new.append(d)
    else:
        return
    desc_method = input("请输入排序方式:1降序，0升序")
    if desc_method == '1':
        desc_method = True
    else:
        desc_method = False
    select = input("请输入排序参照的成绩：1java，2english，3python,0总成绩")
    if select == '1':
        student_new.sort(key=lambda x: int(x['java']), reverse=desc_method)
        formatInfos(student_new)
    elif select == '2':
        student_new.sort(key=lambda x: int(x['english']), reverse=desc_method)
        formatInfos(student_new)
    elif select == '3':
        student_new.sort(key=lambda x: int(x['python']), reverse=desc_method)
        formatInfos(student_new)
    elif select == '0':
        student_new.sort(key=lambda x: int(x['python'])+int(x['java'])+int(x['english']), reverse=desc_method)
        formatInfos(student_new)
    else:
        print("输入有误")
    stu_txt.close()


# 统计学生人数
def total():
    student_list = []
    # 判断文件是否存在
    if os.path.exists(filename):
        stu_txt = open(filename, 'r', encoding="utf-8")
        students = stu_txt.readlines()
        for item in students:
            student_list.append(eval(item))
        if student_list:
            print("一共有" + str(len(student_list)) + "名学生")
        else:
            print("无学生")
        stu_txt.close()


# 显示所有学生信息
def show():
    student_list = []
    # 判断文件是否存在
    if os.path.exists(filename):
        stu_txt = open(filename, 'r', encoding="utf-8")
        students = stu_txt.readlines()
        for item in students:
            student_list.append(eval(item))
        if student_list:
            formatInfos(student_list)
        else:
            print("无学生信息")
        stu_txt.close()


# 格式化显示从文件中读取的信息
def formatInfos(lst):
    # 定义标题
    format_title = "{:^6}\t{:^12}\t{:^8}\t{:^10}\t{:^10}\t{:^8}\t{:^8}\t"
    print(format_title.format("id", "姓名", "性别", "java", "english", "python", "总成绩"))
    # 定义内容显示格式
    format_data = "{:^6}\t{:^12}\t{:^8}\t{:^10}\t{:^10}\t{:^8}\t{:^8}\t"
    for item in lst:
        print(format_data.format(item.get("id"),
                                 item.get("name"),
                                 item.get("sex"),
                                 item.get("java"),
                                 item.get("english"),
                                 item.get("python"),
                                 int(item.get("java")) + int(item.get("english")) + int(item.get("python"))
                                 ))


# 调用菜单函数
menu()
